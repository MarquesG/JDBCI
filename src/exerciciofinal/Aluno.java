/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciofinal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pacote.Conexao;

/**
 *
 * @author gloria
 */
public class Aluno extends Pessoa {

    private String curso;
    private String semestre;
//aaaaaa
    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public void inserir() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String insertTableSQL = "INSERT INTO tab_Pessoa"
                + "(codP,nome,idade,endereco,curso,semestre) VALUES"
                + "(?,?,?,?,?,?)";

        try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(1, this.getCodP());
            prepareStatement.setString(2, this.getNome());
            prepareStatement.setInt(3, this.getIdade());
            prepareStatement.setString(4, this.getEndereco());
            prepareStatement.setString(5, this.getCurso());
            prepareStatement.setString(6, this.getSemestre());

            prepareStatement.executeUpdate();

            System.out.println("ok");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    }

    public void update() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String updateTableSQL = "UPDATE tab_Pessoa SET curso=? semestre=? WHERE codP=?";

        try {
            prepareStatement = dbConnection.prepareStatement(updateTableSQL);

            prepareStatement.setInt(1, this.getCodP());
            prepareStatement.setString(2, this.getNome());
            prepareStatement.setInt(3, this.getIdade());
            prepareStatement.setString(4, this.getEndereco());
            prepareStatement.setString(5, this.getCurso());
            prepareStatement.setString(6, this.getSemestre());
            prepareStatement.executeUpdate();

            System.out.println("updatefunadm");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    }



}
