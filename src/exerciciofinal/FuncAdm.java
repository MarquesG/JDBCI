/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciofinal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pacote.Conexao;

/**
 *
 * @author glori
 */
public class FuncAdm extends Funcionario {

    private String funcao;
    private String setor;

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public void inserir() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String insertTableSQL = "INSERT INTO tab_Pessoa"
                + "(codP,nome,idade,endereco,funcao,setor,salario) VALUES"
                + "(?,?,?,?,?,?,?)";

        try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);

            prepareStatement.setInt(1, this.getIdade());
            prepareStatement.setString(2, this.getNome());
            prepareStatement.setInt(3, this.getIdade());
            prepareStatement.setString(4, this.getEndereco());
            prepareStatement.setString(5, this.getFuncao());
            prepareStatement.setString(6, this.getSetor());
            prepareStatement.setDouble(7, this.getSalario());

            prepareStatement.executeUpdate();

            System.out.println("ok");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    }

    @Override
    public boolean deletar() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String insertTableSQL = "DELETE FROM tab_Pessoa WHERE codP= ?";

        try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);
            prepareStatement.setInt(1, this.codP);

            prepareStatement.executeUpdate();

            System.out.println("okk");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        } finally {
            c.desconecta();
        }
        return true;
    }

    public void update() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String updateTableSQL = "UPDATE tab_Pessoa SET funcao=? setor=? WHERE codP=?";

        try {
            prepareStatement = dbConnection.prepareStatement(updateTableSQL);

            prepareStatement.setInt(1, this.getCodP());
            prepareStatement.setString(2, this.getNome());
            prepareStatement.setInt(3, this.getIdade());
            prepareStatement.setString(4, this.getEndereco());
            prepareStatement.setDouble(5, this.getSalario());
            prepareStatement.setString(6, this.getFuncao());
            prepareStatement.setString(7, this.getSetor());

            prepareStatement.executeUpdate();

            System.out.println("updatefunadm");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    }



}
