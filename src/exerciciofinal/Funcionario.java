/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciofinal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import pacote.Conexao;

/**
 *
 * @author glori
 */
public abstract class Funcionario extends Pessoa {
    private double salario;
    

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    
    
}
