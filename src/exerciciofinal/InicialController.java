package exerciciofinal;

import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author glori
 */
public class InicialController implements Initializable {

    private String funcaoG;
    @FXML
    private TextField setorT;
    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, String> colunaNome;
    @FXML
    private TableColumn<Pessoa, String> colunaEndereco;
    @FXML
    private TableColumn<Pessoa, Integer> colunaIdade;
    @FXML
    private TableColumn<Pessoa, String> colunaFunc;
    @FXML
    private RadioButton prof;
    @FXML
    private RadioButton funcAdm;
    @FXML
    private RadioButton aluno;
    @FXML
    private Label nome;
    @FXML
    private Label end;
    @FXML
    private Label idade;
    @FXML
    private Label funcao;
    @FXML
    private TextField nomeT;
    @FXML
    private TextField endT;
    @FXML
    private TextField idadeT;
    @FXML
    private TextField funcaoT;
    @FXML
    private Label curso;
    @FXML
    private Label semestre;
    @FXML
    private TextField cursoT;
    @FXML
    private TextField semestreT;
    @FXML
    private Label disciplina;
    @FXML
    private Label salario;
    @FXML
    private TextField disciplinaT;
    @FXML
    private TextField salarioT;
    @FXML
    private Label setor;
    @FXML
    private Label codPes;
    @FXML
    private TextField codPesT;

    @FXML
    private TableColumn<Pessoa, Double> colunaSal;
    @FXML
    private TableColumn<Pessoa, String> colunaDisc;
    @FXML
    private TableColumn<Pessoa, String> colunaSetor;
    @FXML
    private TableColumn<Pessoa, String> colunaSem;
    @FXML
    private TableColumn<Pessoa, String> colunaCurso;
    @FXML
    private TableColumn<Pessoa, Integer> colunaCod;

    @FXML
    private Button deletar;
    @FXML
    private Button detalhar;
    @FXML
    private Button att;
    @FXML
    private Button cadastrar;

    private ObservableList<Pessoa> cadastros;
    
    

    @FXML
    private void aluno(ActionEvent ev) {
        funcaoG="Aluno";

        nome.setVisible(true);
        end.setVisible(true);
        idade.setVisible(true);
        semestre.setVisible(true);
        curso.setVisible(true);
        codPes.setVisible(true);

        nomeT.setVisible(true);
        cursoT.setVisible(true);
        semestreT.setVisible(true);
        endT.setVisible(true);
        idadeT.setVisible(true);
        codPesT.setVisible(true);

        disciplina.setVisible(false);
        salario.setVisible(false);

        disciplinaT.setVisible(false);
        salarioT.setVisible(false);

    }

    @FXML
    private void prof(ActionEvent ev) {
        funcaoG="Professor";
        nome.setVisible(true);
        end.setVisible(true);
        idade.setVisible(true);
        salario.setVisible(true);
        disciplina.setVisible(true);
        codPes.setVisible(true);

        nomeT.setVisible(true);
        endT.setVisible(true);
        idadeT.setVisible(true);
        disciplinaT.setVisible(true);
        salarioT.setVisible(true);
        codPesT.setVisible(true);

        semestre.setVisible(false);
        curso.setVisible(false);

        cursoT.setVisible(false);
        semestreT.setVisible(false);

    }

    @FXML
    private void funAdm(ActionEvent ev) {
        funcaoG="Funcionario Adm";

        nome.setVisible(true);
        end.setVisible(true);
        idade.setVisible(true);
        funcao.setVisible(true);
        setor.setVisible(true);
        salario.setVisible(true);
        codPes.setVisible(true);

        nomeT.setVisible(true);
        funcaoT.setVisible(true);
        salarioT.setVisible(true);
        endT.setVisible(true);
        idadeT.setVisible(true);
        setorT.setVisible(true);
        codPesT.setVisible(true);

        semestre.setVisible(false);
        curso.setVisible(false);
        disciplina.setVisible(false);

        cursoT.setVisible(false);
        semestreT.setVisible(false);
        disciplinaT.setVisible(false);
    }

    @FXML
    private void cadastrar(ActionEvent ev) {

        if (funcaoG.equals("Aluno")) {
            Aluno a = new Aluno();
            
            
            a.setNome(nomeT.getText());
            a.setIdade(Integer.parseInt(idadeT.getText()));
            a.setEndereco(endT.getText());
            a.setCurso(cursoT.getText());
            a.setSemestre(semestreT.getText());
            a.setCodP(Integer.parseInt(codPesT.getText()));

            cadastros.add(a);
            a.inserir();

            tabela.setItems(FXCollections.observableList(cadastros));

        }
        if (funcaoG.equals("Professor")) {
            Professor p = new Professor();
            
            
            p.setNome(nomeT.getText());
            p.setIdade(Integer.parseInt(idadeT.getText()));
            p.setEndereco(endT.getText());
            p.setDisciplina(disciplinaT.getText());
            p.setSalario(Double.parseDouble(salarioT.getText()));
            p.setCodP(Integer.parseInt(codPesT.getText()));

            cadastros.add(p);
            p.inserir();

            tabela.setItems(FXCollections.observableList(cadastros));
        }

        if (funcaoG.equals("Funcionario Adm")) {

            FuncAdm func = new FuncAdm();
            
            
            func.setNome(nomeT.getText());
            func.setIdade(Integer.parseInt(idadeT.getText()));
            func.setEndereco(endT.getText());
            func.setSalario(Double.parseDouble(salarioT.getText()));
            func.setSetor(setorT.getText());
            func.setCodP(Integer.parseInt(codPesT.getText()));

            func.inserir();
            cadastros.add(func);

            tabela.setItems(FXCollections.observableList(cadastros));
        }

    }

    @FXML
    private void deletar(ActionEvent ev) {
        Pessoa ps = tabela.getSelectionModel().getSelectedItem();
        if (ps != null) {
            tabela.getItems().remove(ps);
            ps.deletar();
        }
    }

    @FXML
    private void detalhar(ActionEvent ev) {
        colunaSal.setVisible(true);
        colunaCurso.setVisible(true);
        colunaDisc.setVisible(true);
        colunaSetor.setVisible(true);
        colunaSem.setVisible(true);

    }

    @FXML
    private void att(ActionEvent ev) {

        Pessoa ps = tabela.getSelectionModel().getSelectedItem();

        ps.setNome(nome.getText());
        ps.setEndereco(end.getText());
        ps.setIdade(Integer.parseInt(idade.getText()));

        tabela.refresh();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        cadastros = tabela.getItems();

        colunaNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        colunaEndereco.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        colunaIdade.setCellValueFactory(new PropertyValueFactory<>("idade"));
        colunaFunc.setCellValueFactory(new PropertyValueFactory<>("funcao"));
        colunaCurso.setCellValueFactory(new PropertyValueFactory<>("curso"));
        colunaSem.setCellValueFactory(new PropertyValueFactory<>("semestre"));
        colunaSal.setCellValueFactory(new PropertyValueFactory<>("salario"));
        colunaDisc.setCellValueFactory(new PropertyValueFactory<>("disciplina"));
        colunaSetor.setCellValueFactory(new PropertyValueFactory<>("setor"));
        colunaCod.setCellValueFactory(new PropertyValueFactory<>("codP"));
        
        //cadastrar();
        Pessoa p = new Pessoa();
        try {
            List<Pessoa> lista = p.getAll();
            for (Pessoa pes : lista) {

                tabela.getItems().add(pes);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InicialController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
//    private void cadastrar() {
//
//        Aluno a = new Aluno();
//
//        a.setCodP(01);
//        a.setCurso("DS");
//        a.setEndereco("Campas");
//        a.setIdade(18);
//        a.setNome("Glória");
//        a.setSemestre("Segundo");
//        cadastros.add(a);
//
//        a = new Aluno();
//        a.setCodP(02);
//        a.setCurso("Info");
//        a.setEndereco("Canoas");
//        a.setIdade(18);
//        a.setNome("Arthur");
//        a.setSemestre("terceiro");
//        cadastros.add(a);
//
//        a = new Aluno();
//        a.setCodP(03);
//        a.setCurso("Medicina");
//        a.setEndereco("Esteio");
//        a.setIdade(17);
//        a.setNome("Luis");
//        a.setSemestre("quarto");
//        cadastros.add(a);
//
//        Professor p = new Professor();
//
//        p.setCodP(04);
//        p.setDisciplina("Quimica");
//        p.setEndereco("Canoas");
//        p.setIdade(35);
//        p.setNome("Daniela");
//        p.setSalario(14500);
//        cadastros.add(p);
//
//        p = new Professor();
//
//        p.setCodP(05);
//        p.setDisciplina("Matematica");
//        p.setEndereco("POA");
//        p.setIdade(30);
//        p.setNome("Mariana");
//        p.setSalario(15000);
//        cadastros.add(p);
//
//        p = new Professor();
//
//        p.setCodP(06);
//        p.setDisciplina("Portugues");
//        p.setEndereco("Sao Leopoldo");
//        p.setIdade(35);
//        p.setNome("Glaucia");
//        p.setSalario(14000);
//        cadastros.add(p);
//
//        FuncAdm func = new FuncAdm();
//        func.setCodP(07);
//        func.setEndereco("Poa");
//        func.setFuncao("Tecnico");
//        func.setIdade(35);
//        func.setNome("Marcio");
//        func.setSalario(5000);
//        func.setSetor("TI");
//        cadastros.add(func);
//
//        func = new FuncAdm();
//        func.setCodP(8);
//        func.setEndereco("Canoas");
//        func.setFuncao("Diretora");
//        func.setIdade(40);
//        func.setNome("Cris");
//        func.setSalario(4000);
//        func.setSetor("Diretoria");
//        cadastros.add(func);
//
//        func = new FuncAdm();
//        func.setCodP(8);
//        func.setEndereco("Igara");
//        func.setFuncao("Recepcionista");
//        func.setIdade(50);
//        func.setNome("Fulane");
//        func.setSalario(2000);
//        func.setSetor("Recepção");
//        cadastros.add(func);
//    }
}
