/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciofinal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pacote.Conexao;

/**
 *
 * @author glori
 */
public class Pessoa {

    protected int codP;
    private String nome;
    private int idade;
    private String endereco;
    private String funcaoP;

    public String getFuncaoP() {
        return funcaoP;
    }

    public void setFuncaoP(String funcao) {
        this.funcaoP = funcao;
    }

    public int getCodP() {
        return codP;
    }

    public void setCodP(int codP) {
        this.codP = codP;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public boolean deletar() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String insertTableSQL = "DELETE FROM tab_Pessoa WHERE codP= ?";

        try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);
            prepareStatement.setInt(1, this.codP);

            prepareStatement.executeUpdate();

            System.out.println("okk");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        } finally {
            c.desconecta();
        }
        return true;
    }

    public List<Pessoa> getAll() throws SQLException {
        String selectSQL = "SELECT * FROM tab_Pessoa";
        ArrayList<Pessoa> listaPessoa = new ArrayList<>();
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        Statement st;
        try {
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while (rs.next()) {
                if (rs.getString("tipo").equals("Aluno")) {
                    Aluno newAluno = new Aluno();
               
                    newAluno.setCodP(rs.getInt("CodP"));
                    newAluno.setNome(rs.getString("Nome"));
                    newAluno.setEndereco(rs.getString("Endereco"));
                    newAluno.setIdade(rs.getInt("Idade"));
                    newAluno.setCurso(rs.getString("curso"));
                    newAluno.setSemestre(rs.getString("semestre"));
                    newAluno.setFuncaoP(rs.getString("tipo"));
                    listaPessoa.add(newAluno);
                }
                if (rs.getString("tipo").equals("FuncAdm")) {
                    FuncAdm fAdm = new FuncAdm();

                    fAdm.setCodP(rs.getInt("CodP"));
                    fAdm.setNome(rs.getString("Nome"));
                    fAdm.setEndereco(rs.getString("Endereco"));
                    fAdm.setIdade(rs.getInt("Idade"));
                    fAdm.setSalario(rs.getDouble("Salario"));
                    fAdm.setFuncao(rs.getString("funcao"));
                    fAdm.setSetor(rs.getString("setor"));
                    fAdm.setFuncao(rs.getString("tipo"));
                    listaPessoa.add(fAdm);

                }
                if (rs.getString("tipo").equals("Professor")) {
                    Professor prof = new Professor();
                    prof.setCodP(rs.getInt("CodP"));
                    prof.setNome(rs.getString("Nome"));
                    prof.setEndereco(rs.getString("Endereco"));
                    prof.setIdade(rs.getInt("Idade"));
                    prof.setDisciplina(rs.getString("Disciplina"));
                    prof.setSalario(rs.getDouble("Salario"));
                    prof.setFuncaoP(rs.getString("tipo"));
                    

                    listaPessoa.add(prof);
                }
            }
        } catch (SQLException listaP) {
            listaP.printStackTrace();
        }
        return listaPessoa;
    }

}
