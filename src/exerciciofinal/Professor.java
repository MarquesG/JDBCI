/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exerciciofinal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import pacote.Conexao;

/**
 *
 * @author glori
 */
public class Professor extends Funcionario {

    private String disciplina;

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public void inserir() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String insertTableSQL = "INSERT INTO tab_Pessoa"
                + "(codP,nome,idade,endereco,disciplina,salario) VALUES"
                + "(?,?,?,?,?,?)";

        try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);
            prepareStatement.setInt(1, this.getCodP());
            prepareStatement.setString(2, this.getNome());
            prepareStatement.setInt(3, this.getIdade());
            prepareStatement.setString(4, this.getEndereco());
            prepareStatement.setString(5, this.getDisciplina());
             prepareStatement.setDouble(6, this.getSalario());
            prepareStatement.executeUpdate();

            System.out.println("ok");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    }

    @Override
    public boolean deletar() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String insertTableSQL = "DELETE FROM tab_Pessoa WHERE codP= ?";

        try {
            prepareStatement = dbConnection.prepareStatement(insertTableSQL);
            prepareStatement.setInt(1, this.codP);

            prepareStatement.executeUpdate();

            System.out.println("okk");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        } finally {
            c.desconecta();
        }
        return true;
    }

    public void update() {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement prepareStatement = null;

        String updateTableSQL = "UPDATE tab_Pessoa SET disciplina=? WHERE codP=?";

        try {
            prepareStatement = dbConnection.prepareStatement(updateTableSQL);

            prepareStatement.setInt(1, this.getCodP());
            prepareStatement.setString(2, this.getNome());
            prepareStatement.setInt(3, this.getIdade());
            prepareStatement.setString(4, this.getEndereco());
            prepareStatement.setString(5, this.getDisciplina());
             prepareStatement.setDouble(6, this.getSalario());

            prepareStatement.executeUpdate();

            System.out.println("okkk");
        } catch (SQLException e) {
            e.printStackTrace(); //aparecer o erro sql
        }
    }


}
